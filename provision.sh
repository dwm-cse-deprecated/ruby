#! /bin/bash

set -x

# amazon only
cat /etc/system-release | grep Amazon > /dev/null 2>&1
if [ $? -eq 0 ]; then
  yum remove -y ruby20
fi

yum -y install tar make gcc-c++
yum -y install zlib-devel openssl-devel readline-devel libffi-devel patch

cd /usr/local/src
curl -L https://cache.ruby-lang.org/pub/ruby/2.2/ruby-2.2.4.tar.gz -o ruby-2.2.4.tar.gz
tar zxvf ruby-2.2.4.tar.gz
cd ruby-2.2.4
./configure && make && make install

export PATH=$PATH:/usr/local/bin

gem update --system
